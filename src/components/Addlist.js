import { Component } from "react";
import $ from 'jquery';

class Addlist extends Component {
    constructor(props) {
        super(props);

        this.state = {
            time: new Date()
        }
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
      }
    
      componentWillUnmount() {
        clearInterval(this.timerID);
      }
    
      tick = () => {
        this.setState({
          time: new Date()
        });
      }

    onAddListClick = () => {
        console.log('Thêm mới');
        $('#id-ul').append( `<li> ${this.state.time.toLocaleTimeString()} </li> ` )
        
        this.setState({
          
            time: new Date()
        })
    }

    render() {
        return(
            <div className="container text-center">
                <h3>List</h3>
                <ul id="id-ul">

                </ul>
                <p>Its {this.state.time.toLocaleTimeString()} </p>
                <button onClick={this.onAddListClick} className="btn btn-primary">Add to list</button>
            </div>
        )
    }
}

export default Addlist